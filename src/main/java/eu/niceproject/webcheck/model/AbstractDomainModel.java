package eu.niceproject.webcheck.model;

import org.openqa.selenium.support.ui.LoadableComponent;

public abstract class AbstractDomainModel<T extends LoadableComponent<T>> extends LoadableComponent<T> {

    @Override
    protected void load() {

    }

    @Override
    protected void isLoaded() throws Error {

    }
}
