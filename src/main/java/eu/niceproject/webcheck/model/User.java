package eu.niceproject.webcheck.model;

import lombok.Data;

@Data
public class User {

    private String email;
    private String password;
}
