package eu.niceproject.webcheck.runner;

import eu.niceproject.webcheck.utility.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;


public class WebCheckRunner {

    private static final Logger logger = LogManager.getLogger(WebCheckRunner.class);

    public static void main(String[] args) throws IOException {

        logger.info("Info log message");
        JsonParser parser = new JsonParser();
        System.out.println(parser.getJsonFile("src/main/resources/data.json").toString());

    }
}
