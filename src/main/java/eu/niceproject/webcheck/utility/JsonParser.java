package eu.niceproject.webcheck.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.niceproject.webcheck.model.User;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JsonParser {

    public User getJsonFile(String path) throws IOException {

        FileInputStream fis = new FileInputStream(path);
        String file = IOUtils.toString(fis, StandardCharsets.UTF_8);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(file, User.class);
    }
}
