package eu.niceproject.webcheck.utility;

import eu.niceproject.webcheck.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(UserParameterResolver.class)
public class JsonParserTest {

    @InjectMocks
    private JsonParser parser;

    @Spy
    List<User> users = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        User user = new User();
        user.setEmail("michal@123.com");
        user.setPassword("123password");
        users.add(user);
    }

    @Test
    public void parserEmailTest() throws IOException {
        assertEquals(users.get(0).getEmail(), parser.getJsonFile("src/main/resources/data.json").getEmail());
    }

    @Test
    public void parserPasswordTest() throws IOException {
        assertEquals(users.get(0).getPassword(), parser.getJsonFile("src/main/resources/data.json").getPassword());
    }

    @AfterEach
    public void tearDown() {

    }
}
